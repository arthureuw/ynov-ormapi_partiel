<?php

namespace App\DataFixtures;

use App\Entity\Album;
use App\Entity\Artist;
use App\Entity\Style;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder) {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        $faker = (new Factory())::create('fr_FR');

        $artists = [];

        for ($i = 0; $i < 10; $i++) {
            $artist = new Artist();
            $artist->setName($faker->sentence(2, true))
                ->setStartYear($faker->year($max = '2020'));
            $artists[] = $artist;
            $manager->persist($artist);

            $album = new Album();
            $album->setName($faker->sentence(3, true))
                ->setReleaseYear($faker->year('now'))
                ->setArtist($artists[$faker->numberBetween(0, count($artists) - 1)]);
            $manager->persist($album);

            $style = new Style();
            $style->setName($faker->sentence(1, false))
                ->addArtist($artists[$faker->numberBetween(0, count($artists) - 1)]);
            $manager->persist($style);
        }
        $user = new User();
        $user->setEmail('partiel@arthur.com')
            ->setPassword($this->encoder->encodePassword($user, 'panache'));
        $y = $faker->numberBetween(1, 4);
        for ($x = 0; $x < $y; $x++) {
            $user->addArtist($artists[$faker->numberBetween(0, count($artists) - 1)]);
        }
        $manager->persist($user);

        $manager->flush();

    }
}
