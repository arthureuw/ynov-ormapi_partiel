 ## Ynov ORM/API Partiel `16/04/2020`
 
 ### Qu'est-ce qu'un DTO et à quoi sert-il ?
 - Data transfer object, un DTO est utile pour ajouter une partie logique au donnee renvoyer,
    par exemple envoyer l'age d'un user avec seulement ca date de naissance. Le client n'as pas besoin de le calculer.
    
 - Un DTO facilite grandement les transfer de donnee entre les interfaces (back/front ou back/back).

 ### Quelle est la différence entre un listener et un subscriber dans Symfony ?
 - Le Listener détermine l'event via un fichier de config, tandis que le Subscriber le fait dans une méthode.
 - Le premier est donc determine a la compilation tandis que le deuxieme est determine a l'execution et peut etre modifier a la volee
 
 ### Qu'est-ce qu'un JWT ? Pourquoi l'utilise-t-on plutôt que les sessions PHP ?
 - Un JWT est un jeton signe qui permet un echange securise entre plusieur interface. La securite d'un jeton ce fais par la verification de l'integrite des donnees par la signature.
 - En API nous l'utilisons car aucun moyen de session peut etre mise en place du faite que nous ne savons pas avec quelle client l'api dialogue.
  
 ### Qu'est-ce que CORS ?
 - Cross-origin resource sharing, c'est un mechanisme qui est utile dans le cas d'API pour dialoguer avec un domaine exterieur en prevenant l'API de ce meme domaine.
 Nous renseigneront localhost par exemple dans le cas d'un developement local afin de pouvoir echanger avec un navigateur ou le front.
 
 ### Quelle est la différence entre JSON et JSON-LD ?
 - Le JSON-LD est une forme de json mais ajoute des metadonees pour les developpers afin de les aider.
 Par exemple, dans le json d'un User il sera specifie @id pour les donnees correspondante.
